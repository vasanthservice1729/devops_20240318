#### Session Video:

```

```

#### VCS & SCM 

```
VCS / SCM CLI Tool:
    1. git 


VCS / SCM GUI Tools:
    1. GitHub
    2. GitLab
    3. BitBucket

    CloudNative SCM/VCS Tools:

    1. AWS CodeCommit 
    2. Azure Repos



Version Control(VCS) / Source Code Management(SCM) - GIT

1. Getting Started with VCS/SCM
2. What is Git, AWS CodeCommit, and GitHub?
3. About Version Control Systems and Types
4. Git Workflow
5. Installing Git on Windows & Linux
6. Getting Started with Git Commands
7. Working with Branches
8. Merging Branches
9. Creating and Committing a Pull Request
10. Working with Stash

```

```
ubuntu@binarygit:~$ mkdir c3opswebsite

ubuntu@binarygit:~$ pwd
/home/ubuntu

ubuntu@binarygit:~$ ls -lrt
drwxrwxr-x 2 ubuntu ubuntu 4096 Mar 18 04:43 c3opswebsite

ubuntu@binarygit:~$ cd c3opswebsite/

ubuntu@binarygit:~/c3opswebsite$ pwd
/home/ubuntu/c3opswebsite

ubuntu@binarygit:~/c3opswebsite$ ls -lrta
drwxr-x--- 5 ubuntu ubuntu 4096 Mar 18 04:43 ..
drwxrwxr-x 2 ubuntu ubuntu 4096 Mar 18 04:43 .

ubuntu@binarygit:~/c3opswebsite$ git init
hint: Using 'master' as the name for the initial branch. This default branch name
hint: is subject to change. To configure the initial branch name to use in all
hint: of your new repositories, which will suppress this warning, call:
hint: 
hint:   git config --global init.defaultBranch <name>
hint: 
hint: Names commonly chosen instead of 'master' are 'main', 'trunk' and
hint: 'development'. The just-created branch can be renamed via this command:
hint: 
hint:   git branch -m <name>
Initialized empty Git repository in /home/ubuntu/c3opswebsite/.git/
ubuntu@binarygit:~/c3opswebsite$ 

```